:- module(demoserver, []).
:- use_module(library(http/html_head)).
:- use_module(library(http/html_write)).
:- use_module(library(http/js_write)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_error)). % makes error pages show stacktrace
:- use_module(library(http/http_files)).
:- use_module(library(http/http_header)).
:- use_module(library(http/http_parameters)).
:- use_module(library(http/http_json)).
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_log)).
:- use_module(motd).
:- use_module(fancy_box).
:- use_module(random_saying).
:- use_module(cljs_includes).

%% :- debug.

server(Port) :- http_server(http_dispatch, [port(Port)]).

%% Serving files

:- multifile http:location/3.
:- dynamic http:location/3.

http:location(files, '/f', []).

% prefix option means that we'll server anything under assets/
:- http_handler(files(.), http_reply_from_files(assets, []), [prefix]).

% so GET /css/foo.css will look for ./files/css/foo.css
user:file_search_path(css, 'files/css').

user:file_search_path(js, 'files/js').

%% Shared templates

:- html_resource(css('main.css'), [requires(css('reset.css'))]).

header -->
    html([header([img(src='/f/swipl_owl.png'),
                  h1('The Simple Web Page Site')])]).

footer -->
    html([hr([]), footer('Page Footer')]).

:- multifile user:body//2.
user:body(tut_style, Body) -->
    html(body([ \html_requires(css('main.css')),
                \header,
                div(id(content), Body),
                \footer
              ])).

%% Error page

:- multifile http:status_page/3.

http:status_page(not_found(URL), _Context, HTML) :-
    http_log('tried to load invalid URL ~w', [URL]),
    phrase(page(tut_style,
                [title('Not Found')],
                [html([h3('Page Not Found'),
                       p('Couldn\'t find page'-URL)])]),
           HTML).


% XXX: Currently not working -- looks like the status_page hook
% doesn't get invoked for server_errors?
http:status_page(server_error(Error), _Context, HTML) :-
    debug(status_page, 'SERVER ERROR ~w', Error),
    phrase(page(tut_style,
                [title('Something Went Wrong')],
                [html([h3('Server Error'),
                       p('Sorry, something has gone wrong on our end'),
                       code([Error])])]),
           HTML).

%% Main Page

:- http_handler(root(.), main_page, []).

main_page(Request) :-
    http_log('Loading main page', []),
    reply_html_page(
        tut_style,
        [title('Prolog Page')],
        [\page_content(Request)]).

page_content(_Request) -->
    html([h2('The Demo Page'),
          \html_requires(js('foo.cljs')),
          \fancy_border(motd),
          \fancy_border(thing_form),
          \random_saying([demoserver:quote1,
                          demoserver:quote2,
                          demoserver:quote3,
                          demoserver:quote4]),
          h3('Pages'),
          ul([li(a(href(location_by_id(yes_ads)), 'With Ads')),
              li(a(href(location_by_id(broken_page)), 'Broken Page')),
              li(a(href(location_by_id(no_ads)), 'No Ads'))]),
          p('Do some stuff!'),
          \stats_block
         ]).

thing_form -->
    { ActionURL = location_by_id(thing_creator) },
    html([form([action=ActionURL, method='POST'],
               [label(['The thing',
                       input([type=text, name=thing, placeholder=bloop], [])]),
                label(['Number',
                       input([type=number, value=5, name=num_things], [])]),
                input([type=submit, value='GO'], [])])]).

quote1 --> html([p('Prolog is fun.')]).

quote2 --> html([p('LISP is pretty neat.')]).

quote3 --> html([p('Elixir is neat but annoying')]).

quote4 --> html([p('I dislike C++.')]).

%% form handler

:- http_handler(root(create_thing), post_handler, [id(thing_creator)]).

post_handler(Request) :-
    http_parameters(Request,
                    [thing(Thing, [ string ]),
                     num_things(NumThings, [ between(0, 10) ])]),
    reply_html_page(
        tut_style,
        [title('Form Submission')],
        [html([h2('Form Input'),
               p([NumThings, ' ', Thing])])]).


%% Ad/Paid pages

http:location(articles, '/articles', []).

:- http_handler(articles(free), sponsored_page(ads), [id(yes_ads)]).
:- http_handler(articles(paid), sponsored_page(ad_free), [id(no_ads)]).

sponsored_page(HasAds, Request) :-
    reply_html_page(
        tut_style,
        [title('Page with ads')],
        [\ad_page_content(HasAds, Request)]).

ad_page_content(HasAds, _Request) -->
    html([h2('Page with maybe ads'),
          \ad(HasAds),
          p('Here\'s some content'),
          p('Bleep bloop blaap'),
          \ad(HasAds)]).

ad(ad_free) --> html([]).
ad(ads) -->
    {random_member(Sponser, ['Masto', 'Some Person', 'Other'])},
    html([p(['This post brought to you by ', Sponser])]).

%% Broken page

:- http_handler(root(broken), broken_page, [id(broken_page)]).

broken_page(_Request) :-
    %% _ is 1 / 0.
    catch(_ is 1 / 0,
          E,
          (debug(status_page, 'DEBUGGING ~w', [E]),
           throw(http_reply(server_error(E))))).

%% Stats block

http:location(api, '/api', []).

stats_fetch_json -->
    { http_location_by_id(stats_json_handler, Path) },
    js_script(
        {|javascript(Path)||
          function fetchStats() {
              fetch(Path).then(
                (response) => {
                  return response.json();
                }).then(
                  (json) => {
                    let elt = document.getElementById("stats");
                    let txt = `CPU Time: ${json.cpu_time} Code Size: ${json.code_size}`;
                    elt.textContent = txt;
                  });
          }
          setInterval(fetchStats, 5000);
          fetchStats();
         |}).

stats_block --> html([div([id=stats], []), \stats_fetch_json]).

:- http_handler(api(stats), stats_json, [id(stats_json_handler)]).

stats_json(_Request) :-
    statistics(cputime, CpuTime),
    statistics(codes, CodeSize),
    Info = _{code_size: CodeSize, cpu_time: CpuTime},
    reply_json_dict(Info).
