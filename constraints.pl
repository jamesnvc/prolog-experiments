:- use_module(library(clpfd)).

test(X, Y) :-
    X in 0..10,
    Y in 4..8,
    X #> Y.

test2(X, Y) :-
    X in 0..5,
    Y in 4..8,
    X #> Y.


puzzle([S,E,N,D] + [M,O,R,E] = [M,O,N,E,Y]) :-
    Vars = [S,E,N,D,M,O,R,Y],
    Vars ins 0..9,
    all_different(Vars),
    S*1000 + E*100 + N*10 + D +
    M*1000 + O*100 + R*10 + E #=
    M*10000 + O*1000 + N*100 + E*10 + Y,
    %% M #\= 0, S #\= 0,
    label(Vars).

puzzle2([F,O,R,T,Y] + [T,E,N] + [T,E,N] = [S,I,X,T,Y]) :-
    Vars = [F,O,R,T,Y,E,N,S,I,X],
    Vars ins 0..9,
    all_different(Vars),
    F*10000 + O*1000 + R*100 + T*10 + Y +
    T*100 + E*10 + N +
    T*100 + E*10 + N #=
    S*10000 + I*1000 + X*100 + T*10 + Y,
    F #\= 0, T #\= 0, S #\= 0,
    label(Vars).

increase([]) :- true.
increase([_]) :- true.
increase([X,Y|T]) :-
    X #< Y, increase([Y|T]).

%%% Roguelike board checking

board([
[1,1,1,1,1,1,1,1],
[1,0,2,0,0,0,0,0],
[0,1,0,0,0,2,0,0],
[0,0,1,0,0,0,0,3],
[0,0,0,1,0,0,2,0],
[0,0,0,0,1,0,0,0],
[0,0,0,0,0,1,0,0],
[0,0,0,0,0,0,1,0],
[0,0,0,0,0,0,0,1]
]).

%% Helper predicates

length_(Length, List) :- length(List, Length).

% Like nth1/3 for nested lists
nth1s([Idx], List, Val) :-
    nth1(Idx, List, Val).
nth1s([Idx|Idxs], LList, Val) :-
    nth1(Idx, LList, List),
    nth1s(Idxs, List, Val).

% For generating indices of a 2d list
indices(H, W, Row, Col) :-
    between(1, H, Row),
    between(1, W, Col).

%% Checking if a tile is safe (not a wall, not a monster, not next to
%% a monster)

row_safe([], []).
row_safe([_, 2, B|Bt], [0,0,0|Mt]) :-
    row_safe([B|Bt], [0|Mt]).
row_safe([1|Bt], [0|Mt]) :- row_safe(Bt, Mt).
row_safe([3|Bt], [_|Mt]) :- row_safe(Bt, Mt).
row_safe([0|Bt], [_|Mt]) :- row_safe(Bt, Mt).

safe_positions(Board, Safe) :-
    maplist(row_safe, Board, Safe),
    transpose(Board, TransBoard),
    transpose(Safe, TransSafe),
    maplist(row_safe, TransBoard, TransSafe).

%% Finding the distances between the player tile & given tile

player_index(Idx, Row) :-
    element(Idx, Row, 3).
player_index(_, _).

distance(PlayerRow, PlayerCol, Row, Col, D) :-
    D is abs(PlayerRow - Row) + abs(PlayerCol - Col).

distance_for(PlayerRow, PlayerCol, RowIdx, ColIdx, Distances) :-
    distance(PlayerRow, PlayerCol, RowIdx, ColIdx, Dist),
    nth1s([RowIdx, ColIdx], Distances, Dist).

board_distances(Board, Distances) :-
    length(Board, H),
    maplist(length_(W), Board),

    length(Distances, H),
    maplist(length_(W), Distances),

    transpose(Board, TransBoard),
    maplist(player_index(PlayerCol), Board),
    maplist(player_index(PlayerRow), TransBoard),
    !,

    foreach(indices(H, W, Row, Col),
            distance_for(PlayerRow, PlayerCol, Row, Col, Distances)).

%% Putting it together to check board

check_close(0, D) :- D #> 3.
check_close(_, _).

can_move(Board, Moves) :-
    length(Board, H),
    maplist(length_(W), Board),

    length(Moves, H),
    maplist(length_(W), Moves),

    safe_positions(Board, Moves),

    board_distances(Board, Distances),
    maplist(maplist(check_close), Moves, Distances),

    % TODO: need to check that path doesn't move through a monster or wall

    flatten(Moves, FlatMoves),

    FlatMoves ins 0..1,
    % Down means descending possibilities, so if we can't prove it's
    % 0, it's 1
    labeling([down], FlatMoves).

print_row(Row) :-
    write('|'),
    maplist(write, Row),
    write('|'),
    nl.

print_board(Board) :-
    maplist(print_row, Board).

?- board(B), can_move(B,M), nl, nl, print_board(B), nl, print_board(M).
%% ?- board(B), board_distances(B,D), print_board(D).
