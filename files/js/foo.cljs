(ns foo.core)

(defn text-node [txt]
  (.createTextNode js/document txt))

(defn -main []
  (-> (.getElementsByTagName js/document "footer")
      (aget 0)
      (.appendChild
        (doto (.createElement js/document "span")
          (.. -classList  (add "dynamic"))
          (.appendChild (text-node "Hello From ClojureScript!"))))))

(.addEventListener js/window "load" -main)
