:- module(queens, [queens/2]).

:- use_module(library(clpfd)).

pairs(Xs, Pairs) :-
    bagof(X-Y,
          Other^(select(X, Xs, Other),
                 member(Y, Other),
                 X @< Y),
          Pairs).

display_board(Qs) :-
    length(Qs, N),
    foreach(
        between(1, N, Y),
        ( foreach(
              between(1, N, X),
              ( memberchk([X, Y], Qs)
              -> format("♕", [])
              ; 0 is (X + Y) mod 2
                -> format("░", [])
              ; format(" ", [])
              )
           ),
          format("~n", [])
        )
    ).

queens(Qs, N) :-
    length(Qs, N),
    maplist([Q]>>length(Q, 2), Qs),
    bagof(XY, Q^( member(Q, Qs), member(XY, Q) ), Vars),
    Vars ins 1..N,
    pairs(Qs, QPairs),
    maplist(
        [[Q1x,Q1y]-[Q2x,Q2y]]>>(
            Q1x #\= Q2x,
            Q1y #\= Q2y,
            abs(Q1x - Q2x) #\= abs(Q1y - Q2y) ),
        QPairs),
    labeling([ffc], Vars),
    display_board(Qs).
