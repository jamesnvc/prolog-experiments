:- module(persist_server, []).
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/html_write)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_parameters)).
:- use_module(library(persistency)).

:- persistent todos_table(user_id:atom,
                          state:oneof([todo, done]),
                          task:string).

run(Port) :-
    db_attach('todos_data', []),
    http_server(http_dispatch, [port(Port)]).


:- http_handler(root(add), add_handler, []).

add_handler(Request) :-
    member(method(post), Request),
    !,
    http_parameters(Request,
                    [user_id(UserId, [atom]),
                     task(Task, [string])]),
    assert_todos_table(UserId, todo, Task),
    db_sync(_),
    format('Content-Type: text/plain~n~n'),
    format('Added ~w~n', [Task]).

:- http_handler(root(list), list_handler, []).

list_handler(Request) :-
    http_parameters(Request, [user_id(UserId, [atom]),
                              state(State, [oneof([todo, done])])]),
    bagof(Task, todos_table(UserId, State, Task), Tasks),
    format('Content-Type: text/plain~n~n'),
    format('~w tasks~n~w~n', [State, Tasks]).
list_handler(_Request) :-
    format('Content-Type: text/plain~n~n'),
    format('No matching todos.~n').

:- http_handler(root(complete), complete_handler, []).

complete_handler(Request) :-
    member(method(post), Request),
    http_parameters(Request, [user_id(UserId, [atom]),
                              task(Task, [string])]),
    todos_table(UserId, todo, Task),
    assert_todos_table(UserId, done, Task),
    retract_todos_table(UserId, todo, Task),
    db_sync(_),
    format('Content-Type: text/plain~n~n'),
    format('Completed todo~n').
complete_handler(_Request) :-
    format('Content-Type: text/plain~n~n'),
    format('No matching todo.~n').
