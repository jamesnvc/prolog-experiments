:- module(random_saying, [random_saying//1]).
:- use_module(library(http/html_write)).
:- use_module(fancy_box).

random_saying(Sayings) -->
    {random_member(S, Sayings)},
    fancy_border(html([blockquote([\S])])).
