:- module(cljs_includes, []).
:- use_module(library(http/html_head)).
:- use_module(library(http/mimetype)).
:- use_module(library(http/html_write)).

% Make so we can require clojurescript & have it automatically pull in js


:- multifile mime:mime_extension/2.

mime:mime_extension('cljs', 'text/clojurescript').

:- multifile html_head:mime_include//2.

ensure_created(CljsPath, JsPath) :-
    exists_file(JsPath),
    time_file(JsPath, JsTime),
    time_file(CljsPath, CljsTime),
    JsTime >= CljsTime.
ensure_created(CljsPath, _) :-
    !,
    process_create('./build_cljs.sh', [CljsPath], []).

cljs_js(CljsPath, JsPath) :-
    % It seems that we can't just pass the CljsPath in to
    % absolute_file_name/3...js(Path) would work, but this is
    % just the raw path, like '/js/foo.cljs', which fails
    atom_concat(files, CljsPath, CljsFilePath),
    debug(cljs, '[DEBUG] ~w', [CljsFilePath]),
    absolute_file_name(CljsFilePath, CljsFile, [access(read)]),
    !,
    atom_concat(BaseFile, 'cljs', CljsFile),
    atom_concat(BaseFile, 'js', JsFile),
    ensure_created(CljsFile, JsFile),

    atom_concat(BaseName, 'cljs', CljsPath),
    atom_concat(BaseName, 'js', JsPath).

html_head:mime_include('text/clojurescript', Path) -->
    { cljs_js(Path, JsPath) },
    html(script([type('text/javascript'), src(JsPath)], [])).
