#!/usr/bin/env bash

IN_FILE="$1"
OUT_FILE="${2:-${IN_FILE%cljs}js}"
OUT_DIR=$(mktemp -d)

clojure -Sdeps '{:deps {org.clojure/clojurescript {:mvn/version "1.9.946"}}}' \
        -e "(do (require 'cljs.build.api) (cljs.build.api/build \"${IN_FILE}\" {:output-to \"${OUT_FILE}\" :output-dir \"${OUT_DIR}\" :optimizations :advanced}))"
