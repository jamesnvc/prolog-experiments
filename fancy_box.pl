:- module(fancybox, [fancy_border//1]).
:- use_module(library(http/html_write)).

:- html_meta fancy_border(html, ?, ?).

fancy_border(Html) -->
    html([div([style='border: 2px dotted purple'],
              [\Html])]).
