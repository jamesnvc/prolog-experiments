:- module(motd, [motd//0]).
:- use_module(library(http/html_write)).

motd -->
    {random(Msg)},
    html([p(['Message of the Day is ', Msg])]).
